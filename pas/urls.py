from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pas.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # url(r'', include('django_browserid.urls')),
     # url(r'^hello/$', 'users.views.hello_world'),
     url(r'^$', 'users.views.home'),
     url(r'^post/(?P<id>\d+)/$', 'users.views.post_detail', name='post_detail'),
)
